import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Exemplo 2 - Fila Concorrente

        var queueConc:dispatch_queue_t
        queueConc = dispatch_queue_create("com.example.Concurrent", DISPATCH_QUEUE_CONCURRENT)
        
        dispatch_async(queueConc, {
            var i=0
            for (i=0; i<100000; i++){
                print("1")
            }
        })
        
        dispatch_async(queueConc, {
            var i=0
            for (i=0; i<100000; i++){
                print("2")
            }
        })
        
        dispatch_async(queueConc, {
            var i=0
            for (i=0; i<100000; i++){
                print("3")
            }
        })
        
        var i=0
        for (i=0; i<100000; i++){
            print("Main")
        }

    }
    
}

