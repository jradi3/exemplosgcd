import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Exemplo 3 - Fila Serial, sync
        
        var queueSerialSyn:dispatch_queue_t
        queueSerialSyn = dispatch_queue_create("com.example.SerialSync", DISPATCH_QUEUE_SERIAL)
        
        dispatch_sync(queueSerialSyn, {
            var i=0
            for (i=0; i<100000; i++){
                print("1")
            }
        })
        
        print("THREAD PRINCIPAL")
        
        dispatch_sync(queueSerialSyn, {
            var i=0
            for (i=0; i<100000; i++){
                print("2")
            }
        })
        
        print("THREAD PRINCIPAL NOVAMENTE")
        
        dispatch_sync(queueSerialSyn, {
            var i=0
            for (i=0; i<100000; i++){
                print("3")
            }
        })

    }
}

