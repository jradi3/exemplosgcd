import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0) , {
            var i=0
            
            for (i=0; i<100000; i++){
                print("BackGround")
            }
            
            dispatch_async(dispatch_get_main_queue()){
                self.view.backgroundColor = UIColor.redColor()
            }
            
            for (i=0; i<100000; i++){
                print("BackGround 2")
            }
            
        })

    }
}

