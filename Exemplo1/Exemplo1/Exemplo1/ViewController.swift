import UIKit

class ViewController: UIViewController {
    
    func solution(var A : [Int])-> Int {
        // write your code in Objective-C 2.0
        if ( A.count > 0 )
        {
            let N = A.count;
            var Q = 1;
            var B = 0;
            
            while (Q < N) {
                if (Int(A[0]) > Int(A[Q])){
                    let temp = A[0];
                    A[0] = A[Q];
                    A[Q] = temp;
                }
                if (Int(A[Q]) > B){
                    B = A[Q];
                }
                Q++;
                if (Q >= N){
                    return B-A[0];
                }
            }
        }
        return 0;
    }
    
    
//    
//    NSLog("---- A{P} %i", A[P]);
//    NSLog("---- A{Q} %i", A[Q]);
//    
//    NSLog("---- P %i", P);
//    NSLog("---- Q %i", Q);

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Exemplo 1 - Fila Serial, FIFO, async

        
        NSLog("%i ", solution([1,2,44,1,39,44]));

        
        
        
        
        return;
        
        var queueSerial:dispatch_queue_t
        queueSerial = dispatch_queue_create("com.example.Serial", DISPATCH_QUEUE_SERIAL)
        
        dispatch_async(queueSerial, {
            var i=0
            for (i=0; i<200000; i++){
                print("1")
            }
        })
        
        dispatch_async(queueSerial, {
            var i=0
            for (i=0; i<200000; i++){
                print("2")
            }
        })
        
        dispatch_async(queueSerial, {
            var i=0
            for (i=0; i<200000; i++){
                print("3")
            }
        })
        
        var i=0
        for (i=0; i<200000; i++){
            print("Main")
        }

    }
}

